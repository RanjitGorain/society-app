var mongoose = require('mongoose');
var ResidentDocumentSchema = new mongoose.Schema({

    docType: {
        type: String,
        required: true
    },

    note: {
        type: String
    },

    uploadDateTime: {
        type: Date,
        default: Date.now
    },

    userDocKey: {
        type: String
    },

    userDocURL: {
        type: String
    },

    societyId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Society',
        required: true
    },

    addedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        require: true
    },

    forAdmin: {
      type: Boolean,
      default: false
    },

    aws: {
      key: String,
      bucket: String,
      fileName: String // the original file name
    },

    location: String // added 04/10/16 : (AWS S3) url for direct access
});

module.exports = mongoose.model("ResidentDocument", ResidentDocumentSchema);
